/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author bmfil
 */
 
public class dbProducto extends DbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "";
        consulta = "insert into productos(codigo,nombre,fecha,precio,status) values(?,?,?,?,?)";

        if (this.conectar()) {
            try {
                System.out.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al insertar " + e.getMessage());
            }
        }
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;
        String consulta = "";
        consulta = "update productos set nombre = ?,precio=?, fecha =? where codigo =?";
        if (this.conectar()) {
            try {
                System.out.println("Se Actualizo correctamente");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al actualizar " + e.getMessage());
            }
        }
    }

    @Override
    public void habilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;
        String consulta = "update productos set status = 0 where codigo = ?";

        if (this.conectar()) {
            try {
                System.out.println("Se  habilito correctamente");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
               

                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al actualizar estatus" + e.getMessage());
            }
        }
    }

    @Override
    public void deshabilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;
        String consulta = "update productos set status = 1 where codigo = ?";

        if (this.conectar()) {
            try {
                System.out.println("Se desabilito correctamente");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta


                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al actualizar estatus" + e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(int id) throws Exception {
         boolean existe = false;

        if (this.conectar()) {
            String consulta = "";
            consulta = "select * from productos where codigo = ? and status = 0";
            this.sqlConsulta = conexion.prepareStatement(consulta);

            //asignar valores a la consulta
            this.sqlConsulta.setInt(1, id);

            this.registros = this.sqlConsulta.executeQuery();

            //Validamos si existe el id en la BD
            if (this.registros.next()) existe = true;
        }
        this.desconectar();

        return existe;
    }

  

    @Override
    public Object buscar2(String id) throws Exception {
         Productos pro = new Productos();
        if (this.conectar()) {
            try {
                String consulta = "SELECT * FROM productos WHERE codigo = ? AND status = 1";

                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, id);
                this.registros = this.sqlConsulta.executeQuery();

                if (this.registros.next()) {
                    pro.setCodigo(this.registros.getString("codigo"));
                    pro.setPrecio(this.registros.getFloat("precio"));
                    pro.setFecha(this.registros.getString("fecha"));
                    pro.setStatus(this.registros.getInt("status"));
                    pro.setNombre(this.registros.getString("nombre"));
                }
            } catch (Exception e) {
                // Handle any exceptions that occur during the query or result processing
                e.printStackTrace();
            } finally {
                this.desconectar();
            }
        }
        return pro;
    }

    @Override
    public Productos buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if (this.conectar()) {
            try {
                String consulta = "SELECT * FROM productos WHERE codigo = ? AND status = 0";

                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                this.registros = this.sqlConsulta.executeQuery();

                if (this.registros.next()) {
                    pro.setCodigo(this.registros.getString("codigo"));
                    pro.setPrecio(this.registros.getFloat("precio"));
                    pro.setFecha(this.registros.getString("fecha"));
                    pro.setStatus(this.registros.getInt("status"));
                    pro.setNombre(this.registros.getString("nombre"));
                }
            } catch (Exception e) {
                // Handle any exceptions that occur during the query or result processing
                e.printStackTrace();
            } finally {
                this.desconectar();
            }
        }
        return pro;
    }


    @Override
    public DefaultTableModel listar() throws Exception {
         Productos pro;
        if(this.conectar()){ 
            String sql = "select * from sistemas.productos where status = 0 order by codigo";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;       
        }         
        return null;
    }

    @Override
    public DefaultTableModel listar2() throws Exception {
        Productos pro;
        if(this.conectar()){ 
            String sql = "select * from sistemas.productos where status = 1 order by codigo";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;        
        }
        return null;
    }
}


