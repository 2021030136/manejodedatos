/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Modelo;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author bmfil
 */
public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public void habilitar(Object objecto) throws Exception;
    public void deshabilitar(Object objecto) throws Exception;
    
    public boolean isExiste(int  id) throws Exception;
    public DefaultTableModel listar() throws Exception;
    public DefaultTableModel listar2() throws Exception;
    
    public Object buscar2(String id) throws Exception;
    public Object buscar(String codigo) throws Exception;
}

