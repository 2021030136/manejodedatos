package controllers;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import views.jdbProductos;
import Modelo.dbProducto;
import Modelo.Productos;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Controlador implements ActionListener {

    dbProducto db;
    Productos pro;
    jdbProductos vista;
    boolean act;
    boolean hab;
    boolean ena;

    public Controlador(dbProducto db, Productos pro, jdbProductos vista) {
        this.db = db;
        this.pro = pro;
        this.vista = vista;

        vista.btnbuscar.addActionListener(this);
        vista.btncancelar.addActionListener(this);
        vista.btncerrar.addActionListener(this);
        vista.btndesa.addActionListener(this);
        vista.btnguardar.addActionListener(this);
        vista.btnlimpiar.addActionListener(this);
        vista.btnnuevo.addActionListener(this);
        vista.btnbuscar2.addActionListener(this);
        vista.btnhabilitar.addActionListener(this);

    }

    private void iniciarVista() throws Exception {
        vista.tablaactivo.setModel(db.listar());
        vista.tablainactivo.setModel(db.listar2());
        vista.setTitle("productos");
        vista.setSize(800, 700);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnnuevo) {

            vista.txtcodigo.setEnabled(true);
            vista.txtnombre.setEnabled(true);
            vista.txtprecio.setEnabled(true);
            vista.txtcodigo2.setEnabled(true);
            vista.txtnombre2.setEnabled(true);

            vista.btnbuscar.setEnabled(true);
            vista.btndesa.setEnabled(true);
            vista.btnguardar.setEnabled(true);
            vista.btncancelar.setEnabled(true);
            vista.btncerrar.setEnabled(true);
            vista.btnlimpiar.setEnabled(true);
            vista.btnbuscar2.setEnabled(true);
            vista.btnhabilitar.setEnabled(true);

            vista.cldfecha.setEnabled(true);
            vista.tablaactivo.setEnabled(true);
            vista.tablainactivo.setEnabled(true);
        }
        //btn guardar
        if (e.getSource() == vista.btnguardar) {
            if (valiEmpty() != true) {
                JOptionPane.showMessageDialog(vista, "sucecdio un error");
            } else {
                try {
                    if (act != true) {
                        pro = (Productos) db.buscar(vista.txtcodigo.getText());
                        if (pro.getCodigo().equals("")) {
                            pro.setCodigo(vista.txtcodigo.getText());
                            pro.setNombre(vista.txtnombre.getText());
                            pro.setPrecio(Float.parseFloat(vista.txtprecio.getText()));
                            fecha();
                            db.insertar(pro);
                        } else {
                            JOptionPane.showMessageDialog(vista, "codigo existente");
                        }
                    } else {
                        pro.setCodigo(vista.txtcodigo.getText());
                        pro.setNombre(vista.txtnombre.getText());
                        pro.setPrecio(Float.parseFloat(vista.txtprecio.getText()));
                        fecha();
                        db.actualizar(pro);
                        act = false;
                        JOptionPane.showMessageDialog(vista, "Se guardo con exito");

                    }
                    vista.tablaactivo.setModel(db.listar());
                    limpiar();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al guardar: " + ex.getMessage());
                }
            }
        }
        //btn buscar
        if (e.getSource() == vista.btnbuscar) {
            if (vista.txtcodigo.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vista, "proporcione un codigo");
            } else {
                act = true;
                try {
                    pro = (Productos) db.buscar(vista.txtcodigo.getText());
                    vista.txtprecio.setText(String.valueOf(pro.getPrecio()));
                    vista.txtnombre.setText(pro.getNombre());
                    vista.cldfecha.setDate(java.sql.Date.valueOf(pro.getFecha()));
                    vista.txtnombre.setText(pro.getNombre());
                    JOptionPane.showMessageDialog(vista, "Se encontro el producto");

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al buscar los activados: " + ex.getMessage());
                }
            }
        } //btn buscar2
        else if (e.getSource() == vista.btnbuscar2) {
            if (vista.txtcodigo2.getText().isEmpty()) {
                JOptionPane.showMessageDialog(vista, " selecciones un codigo ");
            } else {
                hab = true;
                try {
                    pro = (Productos) db.buscar2(vista.txtcodigo2.getText());
                    vista.txtnombre2.setText(pro.getNombre());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error para los codigod inactivos: " + ex.getMessage());
                }
            }
        }

        //btn desabilitar
        if (e.getSource() == vista.btndesa) {
            if (act != true) {
                JOptionPane.showMessageDialog(vista, "busque un codigo antes de desabilitar");
            } else {
                try {
                    db.deshabilitar(pro);
                    vista.tablaactivo.setModel(db.listar());
                    vista.tablainactivo.setModel(db.listar2());
                    act = false;
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error al deshabilitar: " + ex.getMessage());;
                }

            }
        }
        //btn habilitar
        if (e.getSource() == vista.btnhabilitar) {
            if (hab != true) {
                JOptionPane.showMessageDialog(vista, "busque un codigo antes de habilitar");
            } else {
                try {
                    db.habilitar(pro);
                    vista.tablainactivo.setModel(db.listar2());
                    vista.tablaactivo.setModel(db.listar());
                    hab = false;
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        //btn cancelar

        if (e.getSource() == vista.btncancelar) {
            vista.btnbuscar.setEnabled(false);
            vista.btnguardar.setEnabled(false);
            vista.btnbuscar2.setEnabled(false);
            vista.btncancelar.setEnabled(false);
            vista.btndesa.setEnabled(false);
            vista.btnhabilitar.setEnabled(false);
            vista.btnguardar.setEnabled(false);
            vista.btnlimpiar.setEnabled(false);
            vista.txtcodigo.setEnabled(false);
            vista.txtcodigo2.setEnabled(false);
            vista.txtnombre.setEnabled(false);
            vista.txtnombre2.setEnabled(false);
            vista.txtprecio.setEnabled(false);

            vista.txtcodigo.setText("");
            vista.txtcodigo2.setText("");
            vista.txtnombre.setText("");
            vista.txtnombre2.setText("");
            vista.txtprecio.setText("");
            vista.cldfecha.setDate(null);

        } else if (e.getSource() == vista.btncerrar) {
            if (JOptionPane.showConfirmDialog(vista, "Seguro que desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            } else {
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vista.btnlimpiar || e.getSource() == vista.btnlimpiar2) {
            limpiar();
            act = false;

        }
    }

    public void fecha() {
        int mes = vista.cldfecha.getCalendar().get(Calendar.MONTH) + 1;
        if (mes < 10) {
            pro.setFecha(String.valueOf(vista.cldfecha.getCalendar().get(Calendar.YEAR)) + ",0" + mes + ","
                    + String.valueOf(vista.cldfecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        } else {
            pro.setFecha(String.valueOf(vista.cldfecha.getCalendar().get(Calendar.YEAR)) + "," + mes + ","
                    + String.valueOf(vista.cldfecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
        }
    }

    public void limpiar() {
        vista.txtcodigo.setText("");
        vista.txtcodigo2.setText("");
        vista.txtnombre.setText("");
        vista.txtnombre2.setText("");
        vista.txtprecio.setText("");
        vista.cldfecha.setDate(null);

    }

    public boolean valiEmpty() {
        if (!vista.txtcodigo.getText().isEmpty()
                && !vista.txtnombre.getText().isEmpty()
                && !vista.txtprecio.getText().isEmpty()
                && vista.cldfecha.getDate() != null) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) throws Exception {
        dbProducto db = new dbProducto();
        Productos pro = new Productos();
        jdbProductos vista = new jdbProductos(new JFrame(), true);
        Controlador con = new Controlador(db, pro, vista);
        con.iniciarVista();

    }

}
